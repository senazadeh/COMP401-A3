package a3;

import java.util.Scanner;

public class A3Jedi {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
 		int numberOfIngredients = s.nextInt();
 		IngredientImpl[] ingredients = new IngredientImpl[numberOfIngredients];
 		
 		for (int i = 0; i < numberOfIngredients; i++) {
 			String name = s.next();
 			double pricePerOunce = s.nextDouble();
 			boolean isVegtarian = s.nextBoolean();
 			int caloriesPerOunce = s.nextInt();
 			IngredientImpl ingredient = new IngredientImpl(name, pricePerOunce, caloriesPerOunce, isVegtarian);
 			ingredients[i] = ingredient;
 		}
 		
 		
 		int numberOfRecipes = s.nextInt();
 		MenuItemImpl[] menu = new MenuItemImpl[numberOfRecipes];
 		
 		for (int i = 0; i < numberOfRecipes; i++) {
 	 		String recipeName = s.next();
 	 		int numberOfIngredientsNeeded = s.nextInt();
 	 		IngredientPortion[] portions = new IngredientPortion[numberOfIngredientsNeeded];
 	 		for (int j = 0; j < numberOfIngredientsNeeded; j++) {
 	 			String ingredientName = s.next();
 	 			double ingredientAmount = s.nextDouble();
 	 			for (int k = 0; k < ingredients.length; k++) { 
 	 				if (ingredients[k].getName().equals(ingredientName)) {
 	 	 	 			IngredientPortionImpl ingredientNameAndAmount = new IngredientPortionImpl(ingredients[k], ingredientAmount);
 	 	 	 			portions[j] = ingredientNameAndAmount;
 	 				}
 	 			}
 	 		}
 	 		MenuItemImpl iteam = new MenuItemImpl(recipeName, portions);
 	 		menu[i] = iteam;
 		}
		
 		double[] totalAmountOfEachIngredientNeeded = new double[numberOfIngredients];
 		
 		String nameOfOrder = s.next();
 		while (!nameOfOrder.equals("EndOrder")) {
 	 		for (int i = 0; i < menu.length; i++) {
 	 			if (menu[i].getName().equals(nameOfOrder)) {
 	 				for (int j = 0; j < menu[i].getIngredients().length; j++) {
 	 					for (int k = 0; k < ingredients.length; k++) {
	 	 					if (menu[i].getIngredients()[j].getName().equals(ingredients[k].getName())) {
	 	 						totalAmountOfEachIngredientNeeded[k] += menu[i].getIngredients()[j].getAmount();
	 	 					}
 	 					}
 	 				}
 	 			}
 	 		}
 	 		nameOfOrder = s.next();
 		}
 		s.close();
	
		System.out.println("The order will require:");
		for(int i = 0; i < numberOfIngredients; i++) {
			System.out.println(String.format("%.2f", totalAmountOfEachIngredientNeeded[i]) + " ounces of " + ingredients[i].getName());
 		}
	}
}



