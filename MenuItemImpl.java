package a3;

	public class MenuItemImpl implements MenuItem {
		private String name;
		private IngredientPortion[] ingredients;

		public MenuItemImpl(String name, IngredientPortion[] ingredients) {
			
		if (name == null) {
			throw new RuntimeException("name is null"); }
		if (ingredients == null) {
			throw new RuntimeException("ingredients is null"); }
		if (ingredients.length <= 0) {
			throw new RuntimeException("ingredients length has to be greater than zero"); }
		for (int i = 0; i < ingredients.length; i++) {
			if (ingredients[i] == null) {
				throw new RuntimeException("elements in ingredients[] can not be null"); }}
		
		IngredientPortion[] ingredientsClone = ingredients.clone();
		this.name = name;
		this.ingredients = ingredientsClone;
		}
		
		public String getName() {
			return name;
		}
		
		public IngredientPortion[] getIngredients() {
			return ingredients.clone();
		}

		public int getCalories() {
			int totalCalories = 0;
			for (int i = 0; i < ingredients.length; i++) {
				totalCalories += ingredients[i].getCalories();
			}
			return Math.round(totalCalories);
		}

		public double getCost() {
			double totalCost = 0.00;
			for (int i = 0; i < ingredients.length; i++) {
				totalCost += ingredients[i].getCost();
			}
			return Math.round(totalCost * 100.0) / 100.0;
		}
		
		public boolean getIsVegetarian() {
			boolean isVegetarian = true;
			for (int i = 0; i < ingredients.length; i++) {
				if (ingredients[i].getIsVegetarian() == false) {
					isVegetarian = false;
				}
			}
			return isVegetarian;
		}
}
