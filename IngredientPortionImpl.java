package a3;

	public class IngredientPortionImpl implements IngredientPortion {
		private Ingredient ing;
		private double amount;
		
		public IngredientPortionImpl(Ingredient ing, double amount) {
		
		if (ing.equals(null)) {
			throw new RuntimeException("ing is null"); }
		if (amount < 0) {
			throw new RuntimeException("Amount is negative"); }
		
		this.ing = ing;
		this.amount = amount;
		}

		public Ingredient getIngredient() {
			return ing;
		}

		public double getAmount() {
			return amount;
		}

		public String getName() {
			return ing.getName();
		}

		public boolean getIsVegetarian() {
			return ing.getIsVegetarian();
		}

		public double getCalories() {
			return ing.getCaloriesPerOunce() * amount;
		}

		public double getCost() {
			return ing.getPricePerOunce() * amount;
		}

		public IngredientPortion combine(IngredientPortion other) {
			if (other == null) {
				return this; 
			} else if (!this.getName().equals(other.getName())) {
				throw new RuntimeException("Not the same ingregient"); 
			} else {
				IngredientPortionImpl combined = new IngredientPortionImpl(this.ing, this.getAmount() + other.getAmount());
				return combined;
			}
			
		}
	}	
	
